from home.models import *


def site_variables(request):
    data_dict = {}
    book_obj = None
    about_obj = None
    partner_obj = None
    media_obj = None
    join_obj = None
    contact_obj = None
    movement_obj = None
    whitepaper_obj = None
    document_obj = None

    try:
        book_obj = BookPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        about_obj = AboutPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        partner_obj = PartnerPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        media_obj = MediaPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        join_obj = JoinPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        contact_obj = ContactPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        movement_obj = JoinMovementPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        whitepaper_obj = WhitePaperPage.objects.filter(live=True)[0]
    except IndexError:
        pass

    try:
        document_obj = DocumentPage.objects.all()[0]
    except IndexError:
        pass

    data_dict.update({
        "book": book_obj,
        "about": about_obj,
        "partner": partner_obj,
        'media': media_obj,
        'join': join_obj,
        'contact': contact_obj,
        'movement': movement_obj,
        'white_paper': whitepaper_obj,
        'document_obj': document_obj,
    })

    return data_dict
