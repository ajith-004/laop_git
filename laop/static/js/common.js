$(document).ready(function() {
    // var slides_no = 5;
    // var acc = document.getElementsByClassName("popup_trigger");
    // var i;
    // for (i = 0; i < acc.length; i++) {
    //   acc[i].addEventListener("click", function() {
    //     event.preventDefault();
    //     // $('.text_img').css('display','none');
    //     console.log("***********************************************", $('.text_img'));
    //     this.classList.toggle("active");
    //     var panel = this.nextElementSibling;
    //     $(panel).toggleClass('open');
    //     // console.log("gf",panel);
    //     // if (panel.style.display === "flex") {
    //     //   panel.style.display = "none";
    //     // } else {
    //     //   panel.style.display = "flex";
    //     // }
    //   });
    // }
   $(".join-video-bg").css("display","none");
   $(".join-video-bg").fadeIn(2000);
    $('.home_page_template').show();
    $('.popup_trigger').click(function(event) {
        event.preventDefault();
        var panel = $(this).next('.text_img');
        if($(this).hasClass('active')) {
            $(this).toggleClass('active');
            $(panel).toggleClass('open');
        } else {
            $('.popup_trigger').removeClass('active');
            $('.text_img').removeClass('open');
            $(this).addClass('active');
            $(panel).toggleClass('open');
        }
    });
    function setAspectRatio() {
        $('.bottom_video_section iframe').each(function() {
            $(this).css('height', $(this).width() * 9 / 16);
        });
    }
    setAspectRatio();
    $(window).on('resize load', function() {
       setAspectRatio();
       var url = window.location.href;
       var parts = url.split("#");
       if(parts.length > 1) {
          last_part = parts[parts.length-1];
          if($('#'+last_part)) {
              var scrTop = $('#'+last_part).offset().top - $('header').innerHeight();
              $('html, body').stop().animate({
                  scrollTop: scrTop
              }, 500);
          }
       }
    });

    $(".inputText").each(function(e) {
        $(this).wrap('<fieldset></fieldset>');
        var tag = $(this).attr("placeholder");
          //var tag= $(this).data("tag");
        $(this).attr("placeholder", "");
        $(this).after('<label for="name">' + tag + '</label>');
    });

    $('input').on('blur', function() {
        if (!$(this).val() == "") {
            $(this).next().addClass('stay');
        } else {
            $(this).next().removeClass('stay');
        }
    });
    $('.us_phone').formatter({
        'pattern': '({{999}}) {{999}}-{{9999}}',
        'persistent': false
    });
    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        mobile:       true,       // default
        live:         true        // default
       })
    wow.init();
    var pre_video = $('#modal_video');
    // pre_video.css('display','none');
    var iframe = $('.video_wrapper > iframe');
    if(iframe.length) {
         var player = new Vimeo.Player(iframe);
         player.pause();
         $('.popup_iframe').attr('src', '');

    }
    $("input[type='radio']").click(function(){
        //console.log("enter");
        //var radioValue = $("input[name='radio-group']:checked");
        $('.text_img').removeClass('open');
        $('.popup_trigger').removeClass('active');
        $('.col_5').addClass('hide');
        $('.label').removeClass('active');
        var radio_value = '.' + $(this).attr('id');
        $(radio_value).removeClass('hide');
        var active_value = '.label' + radio_value;
        $(active_value).addClass('active');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(this).offset().top- $('.top_navbar').height()
        },1000);
    });

    $("#contact_form").validate({
        errorPlacement: function (error, element) {
            error.insertBefore(element);
        },
         submitHandler: function (form) {
             $.ajax({
                type: "POST",
                url: $(form).attr('action'),
                data: $(form).serialize(),
                beforeSend: function(){
                    document.getElementById("contact_btn_txt").value="Processing...";
                },
                complete: function(){
                    document.getElementById("contact_btn_txt").value="Submit";
                    $('.pop_up_contact').css('display','block');
                },
                 success: function () {
                    $('.pop_up_contact').css('display','block');
                }
            });
             return false; // required to block normal submit since you used ajax
        },


     });
    $('.pop-up-close').click(function() {
        $('.pop_up').css('display','none');
        $('.pop_up_contact').css('display','none');
        $("#contact_form").trigger( "reset" );
        $('label').removeClass('stay');
        $('body').removeClass('hidden_b');
    });
    $('.download_popup').click(function() {
        $('.pop_up').css('display','block');
        if ($('nav.sidemenu').hasClass('active')) {
            $('nav.sidemenu').removeClass('active');
        }
        $('body').addClass('hidden_b');
    });
 
    var video = $('#initial_video');
    $('.banner_caption span.play').click(function(e) {
        e.stopPropagation();
        var popup_id = $(this).attr('data-id');
        var call_id = $(this).attr('data-class');
        var sol_id = $(this).attr('data-sol');

        // console.log("id",popup_id);
        var popup_link = 'https://player.vimeo.com/video/'+popup_id+'?autoplay=1';
        if(popup_id){
            $('.popup_iframe').attr('src', popup_link);
            $('.call_to_action').attr('data-id',call_id);
            $('.call_to_action').attr('data-class', sol_id);
            //console.log("idthere");
        }
        $('body').addClass('hidden_b');
        //var playing_video = $('.jquery-background-video.test');
        //player.setCurrentTime(0);
        $('.jquery-background-video.test').each(function() {
            $(this).get(0).pause();
        });
        // console.log("before",player.getCurrentTime());
        player.play();
        // console.log("after");
        $('.video_popup_container').addClass('hide');
        var video_obj_id = "." + $(this).attr('data-video');
        //console.log('video_obj_id', video_obj_id);
        $(video_obj_id).removeClass('hide');
        pre_video.css('display','block');
    });
    $('span.icon-close').on('click touchpress', function() {
        pre_video.css('display','none');
        player.pause();
        $('.popup_iframe').attr('src', '');
        $('body').removeClass('hidden_b');
        $('.jquery-background-video.test').each(function() {
            $(this).get(0).play();
        });
    })
    $('.video_thumb_section a').on('click touchpress', function() {
        $('body').addClass('hidden_b');
    });
    $('.sidenav_ul li a').on('click', function(event) {
       var target = $(this).attr('href');
       var link_parts = target.split("#");
       // window.location.hash = '';
       if(link_parts.length > 1) {
          // event.preventDefault();
          $('nav.sidemenu').removeClass('active');
          var last_part = link_parts[link_parts.length-1];
          if($("#"+last_part).length > 0) {
              //console.log("loop Part", $("#"+last_part));
              var scrTop = $("#"+last_part).offset().top - $('header').innerHeight();
              $('html, body').stop().animate({
                  scrollTop: scrTop
              }, 500);
          }
       }
    });
    /*
    $('.call_to_action').on('click', function(event) {
        event.preventDefault();
        pre_video.css('display','none');
        player.pause();
        $('body').removeClass('hidden_b');
        $('.popup_trigger').removeClass('active');
        $('.text_img').removeClass('open');
        var accordion_id = '.trigger'+ $(this).attr('data-id');
        var solution_id = '.col_5.test'+ $(this).attr('data-class');
        $(solution_id).removeClass('hide');
        var active_solution = '#test' + $(this).attr('data-class');
        $(active_solution).click();
        var active_id = '.trigger_active'+ $(this).attr('data-id');
        if ($(this).attr('data-id')){
            var col_id = "#col" + $(this).attr('data-id');
        }
        $(accordion_id).toggleClass('open');
        $(active_id).addClass('active');
        if(col_id) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(col_id).offset().top- $('.top_navbar').height()
            },1000);
        }
        //console.log("acc",accordion_id);
    });*/
    $('.featured_swiper').each(function() {
        var swiper_id = '.'+ $(this).attr('data-id');
        var swiper_class = $(this).attr('data-class');
        if(swiper_class == 'video_slider' ){
            var val=true;
        }
        else {
            var val=false;
        }
        var next = swiper_id + ' .swiper-button-next';
        var prev = swiper_id + ' .swiper-button-prev';
        var mySwiper1 = new Swiper(swiper_id, {
        //mySwiper = new Swiper('.featured_swiper', {
            slidesPerView: 6,
            watchSlidesVisibility:true,
            watchSlidesProgress:true,
            followFinger:false,
            threshold: 10,
            //shortSwipes:false,
            observer:true,
            loop:val,
            //spaceBetween: 2,
            breakpoints: {
            500: {
                slidesPerView: 1,
            },
            600: {
                slidesPerView: 2,
            },
            900: {
                slidesPerView: 3,
            },
            1200: {
                slidesPerView: 4,
            },
            1500: {
                slidesPerView:5,
            }
        },
            navigation: {
                nextEl: next,
                prevEl: prev,
            }
        });
        //console.log("swiper",mySwiper1);
        $(window).on('resize', function() {
            mySwiper1.update();
        });
        $('.season_tab_link').each(function() {
             $(this).click(function(event) {
                event.preventDefault();
                $('.season_tab_link').removeClass('active ');
                $(this).addClass('active');
               $('.first_section').addClass('hide');
                var season_id = '.'+ $(this).attr('data-id');
                //console.log("seasonid",season_id);
                $(season_id).removeClass('hide');
                mySwiper1.update();
                new WOW().init();
                });
        });
        var screenWidth = $(window).width();
        if(screenWidth > 500) {
             mySwiper1.on('slideChange', function () {
                //console.log("size",mySwiper1.clickedIndex);
                $('.swiper-slide').removeClass('slider-scale');
            });
            $('.swiper-slide.team_slider').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    //console.log("clicked");
                    $('.team_slider').removeClass('team_bg');
                    $(this).addClass('team_bg');
                    $('.team_section_data').addClass('hide');
                    var team_id = '.'+ $(this).attr('data-id');
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
            });
            $('.swiper-slide.wh_slider').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    //console.log("clicked");
                    var slider_id = '.'+ $(this).attr('id');
                    var wh_id = '.'+ $(this).attr('data-class');
                    var whp_slider = '.wh_slider'+ slider_id;
                    $(whp_slider).removeClass('team_bg');
                    $(this).addClass('team_bg');
                    var cont_slider = '.team_section_data'+ wh_id;
                    //console.log(cont_slider, "*********");
                    $(cont_slider).addClass('hide');
                    var team_id = '.'+ $(this).attr('data-id');
                    var team_class = team_id + wh_id;
                    $(team_class).removeClass('hide');
                    new WOW().init();
                });
            });
/*            $('.swiper-slide.first-section').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    var swiper_length = $('.first-section.swiper-slide-visible').length;
                    var visible =$('.first-section.swiper-slide-visible')[swiper_length-1];
                    $('.swiper-slide').removeClass('rightend ');
                    $(visible).addClass('rightend');
                    $('.swiper-slide').removeClass('slider-scale ');
                    $(this).addClass('slider-scale ');
                });
            });*/
            $('.swiper-slide.first-section').each(function() {
                 $(this).click(function(event) {
                    //console.log("entered");
                    var data1= $(this);
                    //console.log(data1);
                    var swipe_id = $(data1).attr('data-class');
                    var video_src_id = $(data1).attr('data-video');
                    //console.log("cls",swipe_id);
                    //document.getElementById(swipe_id).addEventListener("click",function(e) {
                       //console.log("seond",e.target);
 /*                       if (e.target && e.target.matches("span.inside_click")) {
                            e.stopPropagation();
                            var popup_id = $(e.target).attr('data-id');
                            var call_id = $(e.target).attr('data-class');
                            var sol_id = $(e.target).attr('data-sol');
                            //console.log("id",popup_id,call_id);
                            var popup_link = 'https://player.vimeo.com/video/'+popup_id+'?autoplay=1';
                            if(popup_id){
                                $('.popup_iframe').attr('src', popup_link);
                                $('.call_to_action').attr('data-id',call_id );
                                $('.call_to_action').attr('data-class', sol_id);
                            }
                            $('body').addClass('hidden_b');
                            //if(video[0]) video[0].pause();
                            player.setCurrentTime(0);
                            window.s = player;
                            player.play();
                            $('.video_popup_container').addClass('hide');
                            var video_obj_id = "." + $(e.target).attr('data-video');
                            //console.log('video_obj_id', video_obj_id);
                            $(video_obj_id).removeClass('hide');
                            pre_video.css('display','block');
                        }*/
                       // else if (e.target && e.target.matches("span.overlay")){
                            //console.log("gsdfgsfg");
                            var swiper_length = $('.first-section.swiper-slide-visible').length;
                            var visible =$('.first-section.swiper-slide-visible')[swiper_length-1];
                            $('.swiper-slide').removeClass('rightend ');
                            $(visible).addClass('rightend');
                            $('.swiper-slide').removeClass('slider-scale ');
                            $('.video_section').removeClass('hide');
                            //console.log("rthis",data1);
                            $(data1).addClass('slider-scale ');
                            $('.jquery-background-video.video_click').addClass('hide').removeClass('test');
                            //console.log("video");
                            var video_id = '.'+ $(data1).attr('data-id');
                            //console.log("vide_id",video_id);
                            var video_section_id = '.jquery-background-video' + video_id;
                            //console.log("bgdhb",video_section_id);
                            $(video_section_id).removeClass('hide').addClass('test');

                           // videosource.setAttribute('src', 'http://www.tools4movies.com/trailers/1012/Kill%20Bill%20Vol.3.mp4');
                            var season_id = $(video_section_id).attr('data-id');
                            var video_source_data = 'video_id_slider ' + swipe_id + ' '+video_src_id ;
                            //console.log("data",video_source_data);
                            var videosource = document.getElementsByClassName(video_source_data);
                            //var total_source = document.getElementsByClassName('video_id_slider');
                            //console.log("fsdf",videosource[0])
                            //$(total_source).attr('src', '');
                            var data_video_src = $(videosource).attr('data-src');
                            if(videosource[0].hasAttribute('src')){
                                //console.log("gdfg");
                            }
                            else{
                                //console.log("aaaaaaaaa");
                                $(videosource[0]).attr('src', data_video_src);
                                var video_source_click = '.jquery-background-video.video_click' + video_id;
                                //console.log("video_source_click",video_source_click);
                                var video = $(video_source_click);
                                video[0].load();
                            }
 
                            //video[0].play();
                            //console.log("season",data_video_src);
                            video_class = '.video_banner' + ' video.' + season_id + '.test';
                            //console.log("cls",video_class);
                            if($(video_class)[0].currentTime != 0){
                                $(video_class)[0].currentTime=0;
                            }
                            $('.banner_caption').addClass('hide');
                            txt_data = '.banner_caption.'  + season_id + video_id;
                            //console.log("txt",txt_data);
                            $(txt_data).removeClass('hide');
                            // $('.jquery-background-video').removeClass('hide');
                        //}
                   // });
                });
            });
        }
        else {
            $('.swiper-slide.first-section').each(function() {
                //console.log("firstsection");
                $('.banner_caption').addClass('hide');
                //txt_data = '.banner_caption.'  + season_id + video_id;
                var team_id = '.'+ $('.first-section.swiper-slide-visible').attr('data-id');
                //console.log("dtaid",team_id);
                var team_value = '.banner_caption'+team_id;
                $(team_value).removeClass('hide');
                player.pause();
                var vid = document.getElementById("initial_video");
                vid.pause();
                $('.jquery-background-video').addClass('hide');
                $('.video_section').addClass('hide');
                var img_section = '.video_section' + team_id;
                $(img_section).removeClass('hide');
            });

            //console.log("mobile");
            $('.swiper_direction').css('display', 'none');
            $('.wh_slider').removeClass('team_bg');
            mySwiper1.on('slideChangeTransitionEnd', function (event) {
               // console.log("swiperid",swiper_id,$(this),$(this)[0].el,"hi",event);
                var data= $(this)[0].el;
                //console.log("fsdfs",data);
                var swipe = data.querySelector('.swipe_btn');
                var swipe_slider = data.querySelector('.swiper-slide.first-section');
                //console.log("hiii",swipe_slider);
                var dup = $('swiper-slide-duplicate')
               // console.log("fafa",swipe);
                $('.swiper-slide').each(function() {
                    //event.preventDefault();
                    $(swipe).css('display', 'none');
                });
                var duplicates =$('.swiper-slide-duplicate')
                for(i=0;i<duplicates.length;i++) {
                    var dat=duplicates[i].querySelector('span.swipe_btn');
                    $(dat).css('display', 'none');
                }
                if (swipe_slider) {
                    $('.swiper-slide.first-section').each(function() {
                        //console.log("firstsection");
                        $('.banner_caption').addClass('hide');
                        //txt_data = '.banner_caption.'  + season_id + video_id;
                        var team_id = '.'+ $('.first-section.swiper-slide-visible').attr('data-id');
                        //console.log("dtaid",team_id);
                        var team_value = '.banner_caption'+team_id;
                        $(team_value).removeClass('hide');
                        player.pause();
                        var vid = document.getElementById("initial_video");
                        vid.pause();
                        $('.jquery-background-video').addClass('hide');
                        $('.video_section').addClass('hide');
                        var img_section = '.video_section' + team_id;
                        $(img_section).removeClass('hide');
                    });
                }
                $('.swiper-slide.team_slider').each(function() {
                    //console.log("en");
                    $('.team_slider').addClass('team_bg');
                    //$(this).addClass('team_bg');
                    $('.team_section_data').addClass('hide');
                    var team_id = '.'+ $('.team_slider.swiper-slide-active').attr('data-id');
                    //console.log("dtaid",team_id);
                    $(team_id).removeClass('hide');
                    new WOW().init();

                });
                $('.swiper-slide.wh_slider').each(function() {
                    // $('.wh_slider').addClass('team_bg');
                    var slider_id = '.'+ $(this).attr('id');
                    var wh_id = '.'+ $(this).attr('data-class');
                    var cont_slider = '.team_section_data'+ wh_id;
                    //console.log("hfghfgh",cont_slider);
                    $(cont_slider).addClass('hide');
                    var team_element = '.wh_slider.swiper-slide-active' + wh_id;
                    var team_id = '.' + $(team_element).attr('data-id');
                    //console.log("dtaid",team_id);
                    var team_class = team_id + wh_id;
                    //console.log("sdjjsdhv", team_class);
                    $(team_class).removeClass('hide');
                    new WOW().init();

                });
                    // var slider_id = '.'+ $(this).attr('id');
                    // var wh_id = '.'+ $(this).attr('data-class');
                    // var whp_slider = '.wh_slider'+ slider_id;
                    // $(whp_slider).removeClass('team_bg');
                    // $(this).addClass('team_bg');
                    // var cont_slider = '.team_section_data'+ wh_id;
                    // console.log(cont_slider, "*********");
                    // $(cont_slider).addClass('hide');
                    // var team_id = '.'+ $(this).attr('data-id');
                    // var team_class = team_id + wh_id;
                    // $(team_class).removeClass('hide');
            });
        }
    });
/*    $('.swiper-slide.first-section').each(function() {
         $(this).click(function(event) {
            event.preventDefault();
            $('.jquery-background-video').addClass('hide').removeClass('test');
            //console.log("video");
            var video_id = '.'+ $(this).attr('data-id');
            //console.log("vide_id",video_id);
            $(video_id).removeClass('hide').addClass('test');
            var season_id = $(video_id).attr('data-id');
            //console.log("season",season_id);
            video_class = '.video_banner' + ' video.' + season_id + '.test';
            //console.log("cls",video_class);
            $(video_class)[0].currentTime=0;
            $('.banner_caption').addClass('hide');
            txt_data = '.banner_caption.'  + season_id + video_id;
            //console.log("txt",txt_data);
            $(txt_data).removeClass('hide');
            // $('.jquery-background-video').removeClass('hide');
        });
    });*/
    $('.toggle_icon').on('click touchpress', function(event) {
        // $('.bg_overlay').css('display', 'block');
        event.preventDefault();
        $('nav.sidemenu').addClass('active');
    });
    $('.close_icon').on('click touchpress', function(event) {
        // $('.bg_overlay').css('display', 'none');
        event.preventDefault();
        if ($('nav.sidemenu').hasClass('active')) {
            $('nav.sidemenu').removeClass('active');
        }
    });
    $('.popup_triggers').each(function(idx,el) {
        $(el).on('click', function(e) {
            e.preventDefault();
            $('body').addClass('hidden_b');
            // $('.modal_popup_wrapper').css('top', e.pageY);
            var tagid = $(this).data('id');
            var contents = $('#' + tagid).html();
            $('.overlay_popup').addClass('active');
            $('.modal_popup_wrapper').addClass('active '+ $(el).attr('data-class'));
            // $('.modal_popup_wrapper').addClass('active');
            $('.modal_popup_wrapper').html(contents);
        });
    });
    $(document).on("click","a.close_ic", function(event) {
        event.preventDefault();
        $('.overlay_popup').removeClass('active');
        $('.modal_popup_wrapper').removeClass().addClass('modal_popup_wrapper');
        $('body').removeClass('hidden_b');
        // $('.overlay_popup').removeClass().addClass('overlay_popup');
    });
    $('.video_category_title').each(function() {
         $(this).click(function(event) {
            event.preventDefault();
            $('.video_wrapper').addClass('hide');
            $('.video_category_title').removeClass('active');
            $(this).addClass('active');
            var video_id = '.'+ $(this).attr('data-id');
            $(video_id).removeClass('hide');
        });
    });
    $('.shifts_swiper').each(function() {
        var swiper_id = '.'+ $(this).attr('data-id');
        if(swiper_id == '.shift_slide1')
            var val=false;
        else
            var val=true;
        var next = swiper_id + ' .swiper-button-next';
        var prev = swiper_id + ' .swiper-button-prev';
        var mySwiper2 = new Swiper(swiper_id, {
        //mySwiper = new Swiper('.featured_swiper', {
            slidesPerView: 5,
            loop:val,
            watchSlidesVisibility:true,
            watchSlidesProgress:true,
            observer:true,
            //spaceBetween: 2,
            breakpoints: {
            500: {
                slidesPerView: 1,
            },
            600: {
                slidesPerView: 2,
            },
            900: {
                slidesPerView: 3,
            },
            1200: {
                slidesPerView: 4,
            },
            1500: {
                slidesPerView:5,
            }
        },
            navigation: {
                nextEl: next,
                prevEl: prev,
            }
        });
        $(window).on('resize', function() {
            mySwiper2.update();
        });
        if(mySwiper2.isBeginning && mySwiper2.isEnd) {
            //$('.swiper_direction.necessary_shifts').css('display', 'none');
        }
        mySwiper2.on('slideChange', function () {
        });
        var screenWidth = $(window).width();
        if(screenWidth > 500) {
            $('.swiper-slide.shift_slider').each(function() {
                $(this).click(function(event) {
                    event.preventDefault();
                    //$('.team_slider').removeClass('team_bg');
                    //$(this).addClass('team_bg');
                    $('.shift_slide').removeClass('active');
                    $('.shift_sections').addClass('hide');
                    //console.log('*********', $(this));
                    var shift_id = '.'+ $(this).attr('data-id');
                    $(shift_id).removeClass('hide');
                    $(this).children().addClass('active');
                    new WOW().init();
                });
            });
            /*
            $('.swiper-slide.roundtable_slider').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    //console.log("clicked");
                    $('.roundtable_slider').removeClass('team_bg');
                    $(this).addClass('team_bg');
                    $('.team_data.roundtable').addClass('hide');
                    var team_id = '.'+ $(this).attr('data-id');
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
            });
            $('.swiper-slide.meetings_slider').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    //console.log("clicked");
                    $('.meetings_slider').removeClass('team_bg');
                    $(this).addClass('team_bg');
                    $('.team_data.meetings').addClass('hide');
                    var team_id = '.'+ $(this).attr('data-id');
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
            });
            $('.swiper-slide.podcasts_slider').each(function() {
                 $(this).click(function(event) {
                    event.preventDefault();
                    //console.log("clicked");
                    $('.podcasts_slider').removeClass('team_bg');
                    $(this).addClass('team_bg');
                    $('.team_data.podcasts').addClass('hide');
                    var team_id = '.'+ $(this).attr('data-id');
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
            });*/
        }
        else {
            mySwiper2.on('slideChangeTransitionEnd', function (event) {
                var data= $(this)[0].el;
                var swipe = data.querySelector('.swipe_btn');
               // console.log("fafa",swipe);
                $('.swiper-slide').each(function() {
                    //event.preventDefault();
                    $(swipe).css('display', 'none');
                });
                 //console.log("dtd",$(this).attr('data-id'),event);
                $('.swiper-slide.shift_slider').each(function() {
                    $('.shift_sections').addClass('hide');
                    //console.log('*********', $(this));
                    var shift_id = '.'+ $('.shift_slider.swiper-slide-active').attr('data-id');
                    //console.log("dtaid",team_id);
                    $(shift_id).removeClass('hide');
                    $('.shift_slide .swipe_btn').css('display', 'none');
                    new WOW().init();
                });
                /*
                $('.swiper-slide.roundtable_slider').each(function() {
                    $('.roundtable_slider').addClass('team_bg');
                    $('.team_data.roundtable').addClass('hide');
                    var team_id = '.'+ $('.roundtable_slider.swiper-slide-active').attr('data-id');
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
                $('.swiper-slide.meetings_slider').each(function() {
                    $('.meetings_slider').addClass('team_bg');
                    $('.team_data.meetings').addClass('hide');
                    var team_id = '.'+ $('.meetings_slider.swiper-slide-active').attr('data-id');
                    console.log("dtaid",team_id);
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
                $('.swiper-slide.podcasts_slider').each(function() {
                    $('.podcasts_slider').addClass('team_bg');
                    $('.team_data.podcasts').addClass('hide');
                    var team_id = '.'+ $('.podcasts_slider.swiper-slide-active').attr('data-id');
                    //console.log("dtaid",team_id);
                    $(team_id).removeClass('hide');
                    new WOW().init();
                });
                */
            });
        }
    });
    $('.video_categories').each(function() {
        $('.'+$('ul li a.video_category_title.active').attr('data-id')).removeClass('hide');
         $(this).click(function(event) {
            event.preventDefault();
            $('.dropdown').toggleClass('hidden');
            // $('.video_mobile_wrapper').addClass('hide');
            $('.title').html($('ul li a.video_category_title.active').html());
            $('.video_mobile_wrapper').addClass('hide');
            $('.'+$('ul li a.video_category_title.active').attr('data-id')).removeClass('hide');
        });
    });
    var isshow = localStorage.getItem('isshow');
    if (isshow== null) {
        localStorage.setItem('isshow', 1);
        $('.pop_up').css('display','block');
        $('body').addClass('hidden_b');
        // Show popup here
        
    }
});

