from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)
from home.models import Season, BackgroundVideos, OurTeam, Medias, MediaCategory, ContactForm, SolutionCategory, DocumentPage, BecomeAdvocateForm
from wagtail.contrib.modeladmin.helpers import PermissionHelper


class ValidationPermissionHelper(PermissionHelper):

    def user_can_list(self, user):
        return True

    def user_can_create(self, user):
        return False


class SeasonAdmin(ModelAdmin):
    model = Season
    menu_label = 'Seasons'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(SeasonAdmin)


class BackgroundVideosAdmin(ModelAdmin):
    model = BackgroundVideos
    menu_label = 'Background Videos'
    menu_order = 200
    add_to_settings_menu = False


modeladmin_register(BackgroundVideosAdmin)


class TeamAdmin(ModelAdmin):
    model = OurTeam
    menu_label = 'Our Team'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(TeamAdmin)


class MediaCategoryAdmin(ModelAdmin):
    model = MediaCategory
    menu_label = 'Media Category'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(MediaCategoryAdmin)


class MediasAdmin(ModelAdmin):
    model = Medias
    menu_label = 'Medias'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(MediasAdmin)


class ContactFormAdmin(ModelAdmin):
    model = ContactForm
    menu_label = "Contact form data"
    menu_icon = 'user'
    menu_order = 300
    add_to_settings_menu = False
    list_display = ('first_name', 'last_name', 'job_title', 'company', 'email', 'phone')
    permission_helper_class = ValidationPermissionHelper


modeladmin_register(ContactFormAdmin)

class AdvocateFormAdmin(ModelAdmin):
    model = BecomeAdvocateForm
    menu_label = "Advocate form data"
    menu_icon = 'user'
    menu_order = 300
    add_to_settings_menu = False
    list_display = ('first_name', 'last_name', 'job_title', 'company', 'email', 'phone')
    permission_helper_class = ValidationPermissionHelper


modeladmin_register(AdvocateFormAdmin)

class SolutionCategoryAdmin(ModelAdmin):
    model = SolutionCategory
    menu_label = 'Solution Category'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(SolutionCategoryAdmin)

class DocumentCategoryAdmin(ModelAdmin):
    model = DocumentPage
    menu_label = 'Document data'
    # menu_icon = 'pilcrow'
    menu_order = 300
    add_to_settings_menu = False


modeladmin_register(DocumentCategoryAdmin)
