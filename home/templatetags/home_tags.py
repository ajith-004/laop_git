from django import template
import re

register = template.Library()


@register.filter
def custom_title(value):
    if not isinstance(value, str):
        value = value.source
    regex = re.compile(r"\*\*(?P<name>.+)\*\*")
    matches = re.search(regex, value)
    if matches:
        name = matches.group('name')
        custom_pos = "**%s**" % (name)
        custom_text = "<span class='special_case'>%s</span>" % (name)
        return value.replace(custom_pos, custom_text)
    else:
        return value
@register.filter
def custom_title_blue(value):
    if not isinstance(value, str):
        value = value.source
    regex = re.compile(r"\*\*\*(?P<name>.+)\*\*\*")
    matches = re.search(regex, value)
    if matches:
        name = matches.group('name')
        custom_pos = "***%s***" % (name)
        custom_text = "<span class='special_case_blue'>%s</span>" % (name)
        return value.replace(custom_pos, custom_text)
    else:
        return value


@register.filter
def consortium_count(value):
    return len(value)


@register.filter
def remquote(value):
    rep_quote = value.source.replace('&quot;', '')
    return rep_quote


@register.filter
def ordersolution(value):
    if value:
        return value.order_by('sort_order')
