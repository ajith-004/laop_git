from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from django import forms
from wagtail.images.blocks import ImageChooserBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.admin.edit_handlers import MultiFieldPanel, StreamFieldPanel, FieldPanel
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core.fields import RichTextField


class HomePage(Page):
    CHOICES = [
        ("m", "Male"),
        ("f", "Female")
    ]
    shift_section_title = models.CharField(max_length=255, null=True)
    shift_section_introduction = models.TextField()
    shift_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('image', DocumentChooserBlock()),
        ('heading', blocks.CharBlock()),
        ('description', blocks.TextBlock()),
        ('forbes_link_description', blocks.TextBlock(required=False)),
        ('forbes_url', blocks.URLBlock(required=False))]))], null=True)
    Where_do_i_start_title = models.CharField(max_length=255, null=True)
    where_do_i_start_introduction = models.TextField()
    laop_consortium_title = models.CharField(null=True, max_length=255)
    laop_consortium_section = StreamField([('image_name_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('name', blocks.CharBlock()),
        ('initial_image', ImageChooserBlock()),
        ('hover_image', ImageChooserBlock()),
        ('sketch_image', ImageChooserBlock()),
        ('brand_identity', blocks.CharBlock()),
        ('designation', blocks.CharBlock(required=False)),
        ('quote', blocks.RichTextBlock(required=False)),
        ('description', blocks.TextBlock()),
        ('solving_for', blocks.TextBlock(required=False)),
        ('gender', blocks.ChoiceBlock(choices=CHOICES, default=CHOICES[0])),
        ('background', blocks.RichTextBlock(required=False))]))], null=True)
    join_the_movement_title = models.CharField(null=True, max_length=255)
    join_the_movement_subtitle = models.CharField(null=True, max_length=255, blank=True)
    join_the_movement_description = RichTextField(null=True, blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('shift_section_title'),
                FieldPanel('shift_section_introduction'),
                StreamFieldPanel('shift_section'),
            ],
            heading='Shift Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('Where_do_i_start_title'),
                FieldPanel('where_do_i_start_introduction'),
            ],
            heading='Where do i start section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('laop_consortium_title'),
                StreamFieldPanel('laop_consortium_section')
            ],
            heading='LAOP Consortium Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('join_the_movement_title'),
                FieldPanel('join_the_movement_subtitle'),
                FieldPanel('join_the_movement_description'),
            ],
            heading='Join the movement section'

        )

    ]

    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        videos = BackgroundVideos.objects.all().order_by('season__name', 'sort_order')
        solutions = Solution.objects.all()
        seasons = Season.objects.all().order_by('-default_season','name')
        solution_category = SolutionCategory.objects.all().order_by('sort_order')
        context.update({
            "videos": videos,
            'solutions': solutions,
            'seasons': seasons,
            'solution_category': solution_category,
        })
        return context

    subpage_types = [
        'home.BookPage',
        'home.AboutPage',
        'home.PartnerPage',
        'home.MediaPage',
        'home.ContactPage',
        'home.JoinPage',
        'home.JoinMovementPage',
        'home.WhitePaperPage',
    ]


@register_snippet
class Solution(models.Model):
    solution_category = models.ForeignKey('SolutionCategory', on_delete=models.SET_NULL, null=True)
    sort_order = models.IntegerField(default=1)
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    button_element = models.TextField(null=True, blank=True)

    panels = [
        FieldPanel('solution_category', widget=forms.Select),
        FieldPanel('sort_order'),
        FieldPanel('title'),
        FieldPanel('description'),
        ImageChooserPanel('image'),
        FieldPanel('button_element'),
    ]

    def __str__(self):
        return self.title + ' - ' + self.solution_category.title

    class Meta:
        verbose_name_plural = "Solutions"


class BackgroundVideos(models.Model):
    season = models.ForeignKey('Season', on_delete=models.SET_NULL, null=True)
    solution = models.ForeignKey('Solution', on_delete=models.SET_NULL, null=True, blank=True)
    sort_order = models.IntegerField(default=1)
    heading = models.CharField(max_length=255)
    sub_description = models.TextField()
    description = models.TextField()
    mobile_slider_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
        help_text='Please upload an image of size 750 x 660 for better view'
    )
    Mp4_video_file = models.FileField(upload_to='uploads/videos', null=True, blank=True)
    Ogv_video_file = models.FileField(upload_to='uploads/videos', null=True, blank=True)
    Webm_video_file = models.FileField(upload_to='uploads/videos', null=True, blank=True)
    popup_vimeo_id = models.CharField(max_length=50, null=True, blank=True)
    popup_youtube_id = models.CharField(max_length=50, null=True, blank=True)
    slider_heading = models.CharField(max_length=255, null=True, blank=True)
    initial_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
        help_text='Please upload an image of size 300 x 169 for better view'
    )
    popup_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
        help_text='Please upload an image of size 300 x 169 for better view'
    )
    time = models.CharField(max_length=50, null=True, blank=True)
    category = models.CharField(max_length=255, null=True, blank=True)
    forbes_link = StreamField([('forbes_title_url', blocks.StructBlock([
        ('title', blocks.CharBlock(required=False)),
        ('forbes_url', blocks.URLBlock(required=False))]))], null=True, blank=True)
    linkedin_link = StreamField([('linkedin_title_url', blocks.StructBlock([
        ('title', blocks.CharBlock(required=False)),
        ('linkedin_url', blocks.URLBlock(required=False))]))], null=True, blank=True)

    panels = [
        FieldPanel('season', widget=forms.Select),
        FieldPanel('solution', widget=forms.Select),
        FieldPanel('sort_order'),
        FieldPanel('heading'),
        FieldPanel('sub_description'),
        FieldPanel('description'),
        ImageChooserPanel('mobile_slider_image'),
        FieldPanel('Mp4_video_file'),
        FieldPanel('Ogv_video_file'),
        FieldPanel('Webm_video_file'),
        FieldPanel('popup_vimeo_id'),
        FieldPanel('popup_youtube_id'),
        FieldPanel('slider_heading'),
        ImageChooserPanel('initial_image'),
        ImageChooserPanel('popup_image'),
        FieldPanel('time'),
        FieldPanel('category'),
        StreamFieldPanel('forbes_link'),
        StreamFieldPanel('linkedin_link')
    ]

    def __str__(self):
        return self.heading + " (" + self.season.name + ") "

    class Meta:
        verbose_name_plural = "Background Videos"


class Season(models.Model):
    name = models.CharField(max_length=125)
    coming_soon_text = models.CharField(max_length=125, null=True, blank=True)
    default_season = models.BooleanField("Make as Default Season",default=False)

    panels = [
        FieldPanel('name'),
        FieldPanel("coming_soon_text"),
        FieldPanel("default_season"),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Seasons"


class BookPage(Page):
    sub_header = models.CharField(max_length=255, null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    listing_page_title = models.TextField()
    listing_page_description = models.TextField()
    listing_page_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    price = models.CharField(max_length=30, null=True, blank=True)
    release_date = models.DateField(null=True, blank=True)
    book_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    vimeo_video_id = models.CharField(max_length=150, null=True, blank=True)
    youtube_video_id = models.CharField(max_length=150, null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("sub_header"),
        FieldPanel("price"),
        FieldPanel("release_date"),
        FieldPanel("description"),
        FieldPanel('listing_page_title'),
        FieldPanel('listing_page_description'),
        ImageChooserPanel("listing_page_image"),
        ImageChooserPanel("book_image"),
        FieldPanel("vimeo_video_id"),
        FieldPanel("youtube_video_id"),
    ]


class AboutPage(Page):
    heading = models.CharField(max_length=150)
    description = models.TextField()

    image = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.CASCADE,
        related_name='+',
        null=True,
        help_text='AboutImage'
    )
    title_with_desc = StreamField([
        ('title_with_desc', blocks.StructBlock([
            ('heading', blocks.TextBlock(required=False)),
            ('description', blocks.TextBlock(required=False)),
        ], icon='user'))
    ], null=True)

    content_panels = Page.content_panels + [
        FieldPanel('heading'),
        FieldPanel('description'),
        ImageChooserPanel('image'),
        StreamFieldPanel('title_with_desc'),

    ]

    def get_context(self, request):
        context = super(AboutPage, self).get_context(request)
        team = OurTeam.objects.all().order_by('sort_order')
        context.update({
            'team': team,

        })
        return context


class OurTeam(models.Model):
    CHOICES = [
        ("m", "Male"),
        ("f", "Female")
    ]
    team_name = models.CharField(max_length=150)
    sort_order = models.IntegerField(default=1)
    team_designation = models.CharField(max_length=255, null=True, blank=True)
    gender = models.CharField(max_length=100, choices=CHOICES, default=CHOICES[0][0])
    initial_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    teamimage_thumbnail = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.CASCADE,
        related_name='+',
        null=True,
        help_text='TeamImage',
        verbose_name='Hover Image'
    )
    teamimage = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.CASCADE,
        related_name='+',
        null=True,
        help_text='TeamsupportingImage',
        verbose_name='Popup Image'
    )
    solving_for = models.TextField(null=True, blank=True)
    background = RichTextField(null=True, blank=True)
    panels = [

        FieldPanel('team_name'),
        FieldPanel('sort_order'),
        FieldPanel('team_designation'),
        ImageChooserPanel('initial_image'),
        ImageChooserPanel('teamimage_thumbnail'),
        ImageChooserPanel('teamimage'),
        FieldPanel("gender", widget=forms.RadioSelect),
        FieldPanel('solving_for'),
        FieldPanel('background'),
    ]

    class Meta:
        verbose_name_plural = "Team Details"

    def __str__(self):
        return self.team_name


class MediaPage(Page):
    description = models.TextField()
    listing_page_title = models.TextField()
    listing_page_description = models.TextField()
    listing_page_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )

    content_panels = Page.content_panels + [
        FieldPanel('description'),
        FieldPanel('listing_page_title'),
        FieldPanel('listing_page_description'),
        ImageChooserPanel("listing_page_image"),
    ]

    def get_context(self, request):
        context = super(MediaPage, self).get_context(request)
        media_obj = Medias.objects.all().order_by('category_id')
        context.update({
            'medias': media_obj,
        })

        return context


class MediaCategory(models.Model):
    name = models.CharField(max_length=150)

    class Meta:
        verbose_name_plural = "Media Categories"

    def __str__(self):
        return self.name


class Medias(models.Model):
    category = models.ForeignKey('MediaCategory', on_delete=models.CASCADE, null=True)
    media_attachments = StreamField([
        ('media_item', blocks.StructBlock([
            ('sort_order', blocks.IntegerBlock()),
            ('media_type', blocks.ChoiceBlock(choices=[
                                              ('image', 'Image'),
                                              ('video', 'Video')], default=[0])),
            ('title', blocks.CharBlock()),
            ('topic', blocks.CharBlock()),
            ('description', blocks.TextBlock()),
            ('video_thumbnail_image', ImageChooserBlock()),
            ('vimeo_video_id', blocks.CharBlock(required=False)),
            ('youtube_video_id', blocks.CharBlock(required=False)),
            ('external_url', blocks.URLBlock(required=False)),
        ])),
    ], null=True, blank=True)

    class Meta:
        verbose_name_plural = "Medias"

    panels = [
        FieldPanel('category', widget=forms.Select),
        StreamFieldPanel('media_attachments'),

    ]
    def __str__(self):
        return 'Pages in '+ self.category.name


class PartnerPage(Page):
    description = models.TextField('Description', null=True, blank=True)
    listing_page_description = models.TextField()
    listing_page_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    partner_details = StreamField([('Partner_details', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('heading', blocks.CharBlock()),
        ('description', blocks.TextBlock()),
        ('forbes_url', blocks.URLBlock()),
        ('image', ImageChooserBlock())]))], null=True)

    content_panels = Page.content_panels + [
        FieldPanel('description'),
        FieldPanel('listing_page_description'),
        ImageChooserPanel('listing_page_image'),
        StreamFieldPanel('partner_details'),
    ]


class ContactForm(models.Model):

    first_name = models.CharField("First Name", max_length=150)
    last_name = models.CharField("Last Name", max_length=150)
    job_title = models.CharField("Job title", max_length=150)
    company = models.CharField("Company", max_length=150)
    email = models.EmailField()
    phone = models.CharField("Phone", max_length=100)
    comment = models.TextField("How did you hear about us")
    message = models.TextField("Message")

    class Meta:
        verbose_name_plural = "Contact Details"

class BecomeAdvocateForm(models.Model):
    first_name = models.CharField("First Name", max_length=150)
    last_name = models.CharField("Last Name", max_length=150)
    job_title = models.CharField("Job title", max_length=150)
    company = models.CharField("Company", max_length=150)
    email = models.EmailField()
    phone = models.CharField("Phone", max_length=100)
    comment = models.TextField("What does personalization mean to you?")
    message = models.TextField("How does personalization influence the ways you work and lead?")
    being_advocate = models.TextField("Why would you like to become an Advocate?")

    class Meta:
        verbose_name_plural = "Become an Advocate"


class ContactPage(Page):
    intro = models.TextField(null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="intro"),
    ]

    def get_context(self, request, *args, **kwargs):
        from home.forms import ContactPageForm
        form = ContactPageForm()
        return {
            'page': self,
            'self': self,
            'form': form,
            'request': request,
        }


class JoinPage(Page):
    intro = models.TextField(null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="intro"),
    ]

    def get_context(self, request, *args, **kwargs):
        from home.forms import AdvocatePageForm
        form = AdvocatePageForm()
        return {
            'page': self,
            'self': self,
            'form': form,
            'request': request,
        }


class SolutionCategory(models.Model):
    sort_order = models.IntegerField()
    title = models.CharField(max_length=255)

    panels = [
        FieldPanel('sort_order'),
        FieldPanel('title'),
    ]

    def __str__(self):
        return self.title


class JoinMovementPage(Page):
    join_section_title = models.CharField(max_length=255, null=True)
    join_section_introduction = models.TextField()
    join_section_description = models.TextField()
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    laop_consortium_image = models.FileField(upload_to='uploads/', null=True, blank=True)
    join_url = models.URLField(null=True, blank=True)
    facebook_url = models.URLField(null=True, blank=True)
    twitter_url = models.URLField(null=True, blank=True)
    linkdln_url = models.URLField(null=True, blank=True)
    youtube_url = models.URLField(null=True, blank=True)
    instagram_url = models.URLField(null=True, blank=True)
    spotify_url = models.URLField(null=True, blank=True)
    vimeo_url = models.URLField(null=True, blank=True)
    itunes_url = models.URLField(null=True, blank=True)
    consortium_title = models.CharField(max_length=255, null=True)
    consortium_introduction = models.TextField()
    consortium_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('title', blocks.CharBlock()),
        ('description', blocks.RichTextBlock(required=False)),
        ('image', DocumentChooserBlock())]))], null=True)
    advocate_section_title = models.CharField(max_length=255, null=True)
    advocate_section_introduction = models.TextField()
    advocate_section_description = StreamField([('description_selection', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('description', blocks.RichTextBlock(required=False)),
        ('LAOP_Advocate', blocks.BooleanBlock(required=False)),
        ('Commited_Enterprise', blocks.BooleanBlock(required=False))]))], null=True)
    members_section_title = models.CharField(max_length=255, null=True)
    members_section_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    roundtables_title = models.CharField(max_length=255, null=True)
    roundtables_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('image', ImageChooserBlock()),
        ('heading', blocks.CharBlock()),
        ('description', blocks.TextBlock()),
        ('apply_url', blocks.URLBlock(required=False))]))], null=True)
    meetings_title = models.CharField(max_length=255, null=True)
    meetings_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('image', ImageChooserBlock()),
        ('heading', blocks.CharBlock()),
        ('description', blocks.TextBlock()),
        ('apply_url', blocks.URLBlock(required=False))]))], null=True)
    podcasts_title = models.CharField(max_length=255, null=True)
    podcasts_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('image', ImageChooserBlock()),
        ('heading', blocks.CharBlock()),
        ('description', blocks.TextBlock()),
        ('apply_url', blocks.URLBlock(required=False))]))], null=True)
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('join_section_title'),
                FieldPanel('join_section_introduction'),
                FieldPanel('join_section_description'),
                ImageChooserPanel('banner_image'),
                FieldPanel('laop_consortium_image'),
                FieldPanel('join_url'),
                FieldPanel('facebook_url'),
                FieldPanel('twitter_url'),
                FieldPanel('linkdln_url'),
                FieldPanel('vimeo_url'),
                FieldPanel('instagram_url'),
                FieldPanel('itunes_url'),
                FieldPanel('spotify_url'),
                FieldPanel('youtube_url'),
            ],
            heading='Join the Movement Consortium Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('consortium_title'),
                FieldPanel('consortium_introduction'),
                StreamFieldPanel('consortium_section'),
            ],
            heading='LAOP Consortium Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('advocate_section_title'),
                FieldPanel('advocate_section_introduction'),
                StreamFieldPanel('advocate_section_description'),
            ],
            heading='Become an Advocate Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('members_section_title'),
                ImageChooserPanel('members_section_image'),
            ],
            heading='Members Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('roundtables_title'),
                StreamFieldPanel('roundtables_section'),
            ],
            heading='Upcoming Roundtables Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('meetings_title'),
                StreamFieldPanel('meetings_section'),
            ],
            heading='Upcoming Consortium Meetings Section'
        ),
        MultiFieldPanel(
            [
                FieldPanel('podcasts_title'),
                StreamFieldPanel('podcasts_section'),
            ],
            heading='Podcasts Section'
        ),
    ]

    def get_context(self, request):
        context = super(JoinMovementPage, self).get_context(request)
        consortium_obj = HomePage.objects.only('laop_consortium_title', 'laop_consortium_section')[0]
        context.update({
            "consortium_obj": consortium_obj,
        })
        return context


class WhitePaperPage(Page):
    description = models.TextField(null=True)
    listing_page_title = models.TextField()
    listing_page_description = models.TextField()
    listing_page_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    whitepaper_section = StreamField([('image_title_description', blocks.StructBlock([
        ('sort_order', blocks.IntegerBlock()),
        ('title', blocks.CharBlock()),
        ('whitepaper_description', blocks.TextBlock()),
        ('banner_image', ImageChooserBlock()),
        ('book_image', ImageChooserBlock()),
        ('document', DocumentChooserBlock()),
        ('slider_section', blocks.StreamBlock([
            ('carousal', blocks.StructBlock([
                ('sort_order', blocks.IntegerBlock()),
                ('image', ImageChooserBlock()),
                ('name', blocks.CharBlock()),
                ('designation', blocks.CharBlock()),
                ('description', blocks.TextBlock()),
                ('user_image', ImageChooserBlock()),
                ('forbes_description', blocks.TextBlock(required=False)),
                ('forbes_url', blocks.URLBlock(required=False))
            ]))]))
    ]))])

    content_panels = Page.content_panels + [
        FieldPanel('description'),
        FieldPanel('listing_page_title'),
        FieldPanel('listing_page_description'),
        ImageChooserPanel('listing_page_image'),
        MultiFieldPanel(
            [
                StreamFieldPanel('whitepaper_section')
            ],
        )
    ]

class DocumentPage(models.Model):
    title = models.CharField(max_length=255, null=True)
    sub_title = models.CharField(max_length=255, null=True)
    description = models.TextField()
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        on_delete=models.CASCADE,
        related_name='+',
    )
    pdf_file = models.FileField(upload_to='uploads/', null=True, blank=True)

    panels = [
        FieldPanel('title'),
        FieldPanel('sub_title'),
        FieldPanel('description'),
        ImageChooserPanel('image'),
        FieldPanel('pdf_file'),
    ]

    def __str__(self):
        return self.title + self.sub_title