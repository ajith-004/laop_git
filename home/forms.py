from django import forms


class ContactPageForm(forms.Form):
    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)
    job_title = forms.CharField(max_length=100, required=False)
    company= forms.CharField(max_length=255, required=False)
    email= forms.EmailField(required=True)
    phone = forms.CharField(max_length=100)
    comment = forms.CharField(widget=forms.Textarea,required=False) 
    message = forms.CharField(widget=forms.Textarea,required=False) 

class AdvocatePageForm(forms.Form):
    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)
    job_title = forms.CharField(max_length=100, required=False)
    company= forms.CharField(max_length=255, required=False)
    email= forms.EmailField(required=True)
    phone = forms.CharField(max_length=100)
    comment = forms.CharField(widget=forms.Textarea,required=False) 
    message = forms.CharField(widget=forms.Textarea,required=False) 
    being_advocate = forms.CharField(widget=forms.Textarea,required=False) 