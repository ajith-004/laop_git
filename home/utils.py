from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


class EmailManager(object):

    def __init__(self, request=None):
        self.request = request
        self.from_address = settings.SERVER_EMAIL

    def send_manager_email(self, subject, options, to, template, bcc=[], fail_silently=False):
        rendered = render_to_string(template, options)
        manager_email = EmailMessage(subject=subject,
                                     body=rendered,
                                     from_email=self.from_address,
                                     to=to,
                                     bcc=bcc)
        manager_email.content_subtype = "html"
        manager_email.send()

