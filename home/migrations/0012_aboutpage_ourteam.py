# Generated by Django 2.2.9 on 2020-01-24 05:32

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0041_group_collection_permissions_verbose_name_plural'),
        ('wagtailimages', '0001_squashed_0021'),
        ('home', '0011_bookpage'),
    ]

    operations = [
        migrations.CreateModel(
            name='OurTeam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('team_name', models.CharField(max_length=150)),
                ('sort_order', models.IntegerField(default=1)),
                ('team_designation', models.CharField(blank=True, max_length=255, null=True)),
                ('gender', models.CharField(choices=[('m', 'Male'), ('f', 'Female')], default='m', max_length=100)),
                ('solving_for', models.TextField(blank=True, null=True)),
                ('background', wagtail.core.fields.RichTextField(blank=True, null=True)),
                ('initial_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.Image')),
                ('teamimage', models.ForeignKey(help_text='TeamsupportingImage', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.Image', verbose_name='Popup Image')),
                ('teamimage_thumbnail', models.ForeignKey(help_text='TeamImage', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.Image', verbose_name='Hover Image')),
            ],
            options={
                'verbose_name_plural': 'Team Details',
            },
        ),
        migrations.CreateModel(
            name='AboutPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('heading', models.CharField(max_length=150)),
                ('description', models.TextField()),
                ('title_with_desc', wagtail.core.fields.StreamField([('title_with_desc', wagtail.core.blocks.StructBlock([('heading', wagtail.core.blocks.TextBlock(required=False)), ('description', wagtail.core.blocks.TextBlock(required=False))], icon='user'))], null=True)),
                ('image', models.ForeignKey(help_text='AboutImage', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.Image')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
    ]
