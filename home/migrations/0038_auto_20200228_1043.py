# Generated by Django 2.2.9 on 2020-02-28 10:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0001_squashed_0021'),
        ('home', '0037_auto_20200224_0626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='joinmovementpage',
            name='laop_consortium_image',
            field=models.FileField(blank=True, null=True, upload_to='uploads/'),
        ),
        migrations.CreateModel(
            name='DocumentPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, null=True)),
                ('sub_title', models.CharField(max_length=255, null=True)),
                ('description', models.TextField()),
                ('pdf_file', models.FileField(blank=True, null=True, upload_to='uploads/')),
                ('image', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.Image')),
            ],
        ),
    ]
