# Generated by Django 2.2.9 on 2020-01-21 07:10

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20200116_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='laop_consortium_section',
            field=wagtail.core.fields.StreamField([('image_name_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('name', wagtail.core.blocks.CharBlock()), ('initial_image', wagtail.images.blocks.ImageChooserBlock()), ('hover_image', wagtail.images.blocks.ImageChooserBlock()), ('sketch_image', wagtail.images.blocks.ImageChooserBlock()), ('image_caption', wagtail.core.blocks.CharBlock()), ('designation', wagtail.core.blocks.CharBlock(required=False)), ('description', wagtail.core.blocks.TextBlock()), ('brand_identity', wagtail.core.blocks.CharBlock(required=False)), ('solving_for', wagtail.core.blocks.TextBlock(required=False)), ('gender', wagtail.core.blocks.ChoiceBlock(choices=[('m', 'Male'), ('f', 'Female')])), ('background', wagtail.core.blocks.RichTextBlock(required=False))]))], null=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='shift_section',
            field=wagtail.core.fields.StreamField([('image_title_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('heading', wagtail.core.blocks.CharBlock()), ('description', wagtail.core.blocks.TextBlock()), ('forbes_link_description', wagtail.core.blocks.TextBlock()), ('forbes_url', wagtail.core.blocks.URLBlock())]))], null=True),
        ),
    ]
