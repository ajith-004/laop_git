# Generated by Django 2.2.9 on 2020-02-13 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0031_auto_20200213_0606'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solution',
            name='phone',
        ),
        migrations.AddField(
            model_name='solution',
            name='button_element',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
