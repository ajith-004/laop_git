# Generated by Django 2.2.9 on 2020-02-13 09:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0033_auto_20200213_0704'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solution',
            name='video',
        ),
        migrations.RemoveField(
            model_name='solutioncategory',
            name='solution',
        ),
        migrations.AddField(
            model_name='backgroundvideos',
            name='solution',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='home.Solution'),
        ),
        migrations.AddField(
            model_name='solution',
            name='solution_category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='home.SolutionCategory'),
        ),
    ]
