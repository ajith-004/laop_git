# Generated by Django 2.2.9 on 2020-02-13 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0032_auto_20200213_0654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='button_element',
            field=models.TextField(blank=True, null=True),
        ),
    ]
