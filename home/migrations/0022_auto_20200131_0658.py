# Generated by Django 2.2.9 on 2020-01-31 06:58

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0021_auto_20200130_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backgroundvideos',
            name='forbes_link',
            field=wagtail.core.fields.StreamField([('forbes_title_url', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(required=False)), ('forbes_url', wagtail.core.blocks.URLBlock(required=False))]))], blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='backgroundvideos',
            name='linkedin_link',
            field=wagtail.core.fields.StreamField([('linkedin_title_url', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(required=False)), ('linkedin_url', wagtail.core.blocks.URLBlock(required=False))]))], blank=True, null=True),
        ),
    ]
