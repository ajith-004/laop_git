# Generated by Django 2.2.9 on 2020-02-06 09:11

from django.db import migrations, models
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.documents.blocks
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0026_auto_20200205_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='joinmovementpage',
            name='advocate_section_description',
            field=wagtail.core.fields.StreamField([('description_selection', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('description', wagtail.core.blocks.RichTextBlock(required=False)), ('LAOP_Advocate', wagtail.core.blocks.BooleanBlock(required=False)), ('Commited_Enterprise', wagtail.core.blocks.BooleanBlock(required=False))]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='advocate_section_introduction',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='advocate_section_title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='consortium_introduction',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='consortium_section',
            field=wagtail.core.fields.StreamField([('image_title_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('title', wagtail.core.blocks.CharBlock()), ('description', wagtail.core.blocks.RichTextBlock(required=False)), ('image', wagtail.documents.blocks.DocumentChooserBlock())]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='consortium_title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='meetings_section',
            field=wagtail.core.fields.StreamField([('image_title_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('heading', wagtail.core.blocks.CharBlock()), ('description', wagtail.core.blocks.TextBlock()), ('apply_url', wagtail.core.blocks.URLBlock(required=False))]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='meetings_title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='members_section_description',
            field=wagtail.core.fields.StreamField([('description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False))]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='members_section_title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='podcasts_section',
            field=wagtail.core.fields.StreamField([('image_title_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('heading', wagtail.core.blocks.CharBlock()), ('description', wagtail.core.blocks.TextBlock()), ('apply_url', wagtail.core.blocks.URLBlock(required=False))]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='podcasts_title',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='roundtables_section',
            field=wagtail.core.fields.StreamField([('image_title_description', wagtail.core.blocks.StructBlock([('sort_order', wagtail.core.blocks.IntegerBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('heading', wagtail.core.blocks.CharBlock()), ('description', wagtail.core.blocks.TextBlock()), ('apply_url', wagtail.core.blocks.URLBlock(required=False))]))], null=True),
        ),
        migrations.AddField(
            model_name='joinmovementpage',
            name='roundtables_title',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
