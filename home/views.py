from django.views.generic import FormView
from django.contrib import messages
from .forms import ContactPageForm,AdvocatePageForm
from home.models import ContactForm, ContactPage, BecomeAdvocateForm
# from django.http import JsonResponse
from home.utils import EmailManager
from django.conf import settings


class ContactSubmissionView(FormView):
    form_class = ContactPageForm
    template_name = "home/contact_page.html"
    manager_mail_subject = "Confirmation Email – Automatic Email"
    manager_mail_template = "home/email/contact_page_details.html"
    manager_subject = "LAOP - Contact Form"
    manager_data_template = "home/email/manager_email_content.html"

    def get_success_url(self):
        page_url = ContactPage.objects.filter(live=True)[0].url
        return page_url

    def form_valid(self, form):
        contact_form_obj = ContactForm()
        contact_form_obj.first_name = form.cleaned_data['first_name']
        contact_form_obj.last_name = form.cleaned_data['last_name']
        contact_form_obj.job_title = form.cleaned_data['job_title']
        contact_form_obj.company = form.cleaned_data['company']
        contact_form_obj.email = form.cleaned_data['email']
        contact_form_obj.phone = form.cleaned_data['phone']
        contact_form_obj.comment = form.cleaned_data['comment']
        contact_form_obj.message = form.cleaned_data['message']
        contact_form_obj.save()
        mail_context = {}
        self.email_manager = EmailManager(request=self.request)
        mail_context.update({
            "form": form,
        })
        send_mail = [ contact_form_obj.email ]
        self.email_manager.send_manager_email(subject=self.manager_subject,
                                              options=mail_context,
                                              to=settings.MANAGER_LIST,
                                              template=self.manager_data_template,
                                              )
        return super(ContactSubmissionView, self).form_valid(form)

    def form_invalid(self, form):
        return super(ContactSubmissionView, self).form_invalid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            #messages.add_message(request, messages.INFO, "Thank you for contacting us")
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

class AdvocateSubmissionView(FormView):
    form_class = AdvocatePageForm
    template_name = "home/join_page.html"
    manager_mail_subject = "LAOP Advocate Application Confirmation"
    manager_mail_template = "home/email/contact_page_details.html"
    manager_subject = "LAOP - Advocate Application Form"
    manager_data_template = "home/email/advocate_email.html"

    def get_success_url(self):
        page_url = ContactPage.objects.filter(live=True)[0].url
        return page_url

    def form_valid(self, form):
        contact_form_obj = BecomeAdvocateForm()
        contact_form_obj.first_name = form.cleaned_data['first_name']
        contact_form_obj.last_name = form.cleaned_data['last_name']
        contact_form_obj.job_title = form.cleaned_data['job_title']
        contact_form_obj.company = form.cleaned_data['company']
        contact_form_obj.email = form.cleaned_data['email']
        contact_form_obj.phone = form.cleaned_data['phone']
        contact_form_obj.comment = form.cleaned_data['comment']
        contact_form_obj.message = form.cleaned_data['message']
        contact_form_obj.being_advocate = form.cleaned_data['being_advocate']
        contact_form_obj.save()
        mail_context = {}
        self.email_manager = EmailManager(request=self.request)
        mail_context.update({
            "form": form,
        })
        send_mail = [ contact_form_obj.email ]
        self.email_manager.send_manager_email(subject=self.manager_mail_subject,
                                              options=mail_context,
                                              to=send_mail,
                                              template=self.manager_mail_template,
                                              )
        self.email_manager.send_manager_email(subject=self.manager_subject,
                                              options=mail_context,
                                              to=settings.MANAGER_LIST,
                                              template=self.manager_data_template,
                                              )
        return super(ContactSubmissionView, self).form_valid(form)

    def form_invalid(self, form):
        return super(ContactSubmissionView, self).form_invalid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            #messages.add_message(request, messages.INFO, "Thank you for contacting us")
            return self.form_valid(form)
        else:
            return self.form_invalid(form)